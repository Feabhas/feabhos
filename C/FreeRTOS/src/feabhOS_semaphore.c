// -------------------------------------------------------------------------------------
//  FeabhOS OS abstraction layer
//
//  DISCLAIMER:
//  Feabhas is furnishing this item "as is". Feabhas does not provide any warranty
//  of the item whatsoever, whether express, implied, or statutory, including, but
//  not limited to, any warranty of merchantability or fitness for a particular
//  purpose or any warranty that the contents of the item will be error-free.
//  In no respect shall Feabhas incur any liability for any damages, including, but
//  limited to, direct, indirect, special, or consequential damages arising out of,
//  resulting from, or any way connected to the use of the item, whether or not
//  based upon warranty, contract, tort, or otherwise; whether or not injury was
//  sustained by persons or property or otherwise; and whether or not loss was
//  sustained from, or arose out of, the results of, the item, or any services that
//  may be provided by Feabhas.
//
// -------------------------------------------------------------------------------------

#include "feabhOS_semaphore.h"
#include "feabhOS_defs.h"
#include <assert.h>

typedef struct feabhOS_semaphore
{
  OS_BINARY_SEMAPHORE_TYPE handle;
  
} feabhOS_semaphore_t;


// ----------------------------------------------------------------------------
//
//  MEMORY MANAGEMENT FOR SEMAPHORE STRUCTURES
//  ------------------------------------------
//
//  for a FreeRTOS implementation we don't
//  allow dynamic allocation of semaphore structs.
//  This is to keep control of heap memory in our
//  limited-resource system.
//  If the client has set MAX_SEMAPHORES to NO_LIMIT
//  we need to stop compilation with an error.

#if MAX_SEMAPHORES==NO_LIMIT

#error "You must not set MAX_SEMAPHORES to NO_LIMIT with FreeRTOS"

#endif


//  Semaphore structure static allocation.
//  For simplicity, every time a semaphore is 'created'
//  by the client the next element in the array is
//  used; until they are all gone.
//  There is no re-use of semaphores in this implementation.
//
//  If all semaphores are taken the code will assert.
//
static feabhOS_SEMAPHORE get_instance(void)
{
  static feabhOS_semaphore_t instances[MAX_SEMAPHORES];
  static unsigned int next = 0;

  assert(next != MAX_SEMAPHORES);

  return &instances[next++];
}



feabhOS_error feabhOS_semaphore_create(feabhOS_SEMAPHORE * const semaphore_handle)
{
  feabhOS_SEMAPHORE semaphore;

  semaphore = get_instance();

  vSemaphoreCreateBinary(semaphore->handle);
  if(semaphore->handle == 0) return ERROR_OUT_OF_MEMORY;
  
  *semaphore_handle = semaphore;
  return ERROR_OK;
}


feabhOS_error feabhOS_semaphore_take(feabhOS_SEMAPHORE * const semaphore_handle, duration_mSec_t timeout)
{
  // Parameter checking:
  //
  if(semaphore_handle == NULL) return ERROR_INVALID_HANDLE;

  feabhOS_SEMAPHORE semaphore = *semaphore_handle;
  OS_ERROR_TYPE OSError;

  OSError = xSemaphoreTake(semaphore->handle, (OS_TIME_TYPE)timeout);
  
  if (OSError == pdPASS) return ERROR_OK;
  else                   return ERROR_TIMED_OUT;
}


feabhOS_error feabhOS_semaphore_give(feabhOS_SEMAPHORE * const semaphore_handle)
{
  // Parameter checking:
  //
  if(semaphore_handle == NULL) return ERROR_INVALID_HANDLE;

  feabhOS_SEMAPHORE semaphore = *semaphore_handle;
  xSemaphoreGive(semaphore->handle);

  return ERROR_OK;
}


feabhOS_error feabhOS_semaphore_destroy(feabhOS_SEMAPHORE * const semaphore_handle)
{
  // Parameter checking:
  //
  if(semaphore_handle == NULL) return ERROR_INVALID_HANDLE;

  feabhOS_SEMAPHORE semaphore = *semaphore_handle;

  // No clean-up in FreeRTOS.

  // Ensure the client's handle is invalid
  //
  semaphore->handle = NULL;

  return ERROR_OK;
}

