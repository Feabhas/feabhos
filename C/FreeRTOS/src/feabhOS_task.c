// -------------------------------------------------------------------------------------
//  FeabhOS OS abstraction layer
//
//  DISCLAIMER:
//  Feabhas is furnishing this item "as is". Feabhas does not provide any warranty
//  of the item whatsoever, whether express, implied, or statutory, including, but
//  not limited to, any warranty of merchantability or fitness for a particular
//  purpose or any warranty that the contents of the item will be error-free.
//  In no respect shall Feabhas incur any liability for any damages, including, but
//  limited to, direct, indirect, special, or consequential damages arising out of,
//  resulting from, or any way connected to the use of the item, whether or not
//  based upon warranty, contract, tort, or otherwise; whether or not injury was
//  sustained by persons or property or otherwise; and whether or not loss was
//  sustained from, or arose out of, the results of, the item, or any services that
//  may be provided by Feabhas.
//
// -------------------------------------------------------------------------------------

#include "feabhOS_task.h"
#include "FreeRTOS.h"
#include "task.h"
#include <assert.h>

// ----------------------------------------------------------------------------
// Static function prototypes
//
static void scheduled_function(void *arg);
static feabhOS_TASK get_instance(void);

//  Although FeabhOS uses the same function signature as FreeRTOS this
//  is not always guaranteed.  Sometimes a port requires a different
//  function signature for its threads.  To hide this we  wrap the client's
//  function and parameter into a structure and pass this structure to an
//  intermediary function with the correct signature for the underlying OS.
//
typedef struct user_code
{
  void (*function)(void*);
  void *parameter;

} user_code_t;


typedef struct feabhOS_task
{
  OS_TASK_TYPE handle;
  user_code_t  user_code;

} feabhOS_task_t;


// ----------------------------------------------------------------------------
//
//	MEMORY MANAGEMENT FOR TASK STRUCTURES
//  -------------------------------------
//
//	for a FreeRTOS implementation we don't
//	allow dynamic allocation of task structs.
//	This is to keep control of heap memory in our
//	limited-resource system.
//  If the client has set MAX_TASKS to NO_LIMIT
//	we need to stop compilation with an error.

#if MAX_TASKS==NO_LIMIT

#error "You must not set MAX_TASKS to NO_LIMIT with FreeRTOS"

#endif


//  Task structure static allocation.
//  For simplicity, every time a task is 'created'
//  by the client the next element in the array is
//  used; until they are all gone.
//  There is no re-use of tasks in this implementation.
//
//  If all tasks are taken the code will assert.
//
static feabhOS_TASK get_instance(void)
{
  static feabhOS_task_t tasks[MAX_TASKS];
  static unsigned int next_task = 0;

  assert(next_task != MAX_TASKS);

  return &tasks[next_task++];
}

// ----------------------------------------------------------------------------
//
feabhOS_error feabhOS_task_create(feabhOS_TASK * const  task_handle,
                                  void (*function)(void*),
                                  void *                param, 
                                  feabhOS_stack_size_t  stack,
                                  feabhOS_priority_t    priority)
{
  feabhOS_TASK  task;
  OS_ERROR_TYPE OS_error;

  // Parameter checks:
  //
  if(function == NULL)                                              return ERROR_PARAM1;
  if((stack < STACK_TINY) || (stack > STACK_HUGE))                  return ERROR_PARAM3;
  if((priority < PRIORITY_LOWEST) || (priority > PRIORITY_HIGHEST)) return ERROR_PARAM4;
	
  task = get_instance();

  task->user_code.function  = function;
  task->user_code.parameter = param;

  OS_error = xTaskCreate(scheduled_function,
                         "FeabhOS task",
                         (portSHORT)(stack / sizeof(StackType_t)),
                         task,
                         (portBASE_TYPE)priority,
                         &task->handle);

  // The OS will fail if it cannot allocate memory
  // for (its own) control structures.
  //
  if(OS_error != pdPASS) return ERROR_OUT_OF_MEMORY;
  
  *task_handle = task;
  return ERROR_OK;
}


void scheduled_function(void *arg)
{
  feabhOS_TASK task = (feabhOS_TASK)arg;
  task->user_code.function(task->user_code.parameter);

  // The user code function should never exit.
  // However, if it does it can crash the underlying
  // OS.  Therefore, if we get to this point we should
  // self-terminate.
  //
  feabhOS_task_destroy(&task);
}


feabhOS_error feabhOS_task_setPriority(feabhOS_TASK * const task_handle, feabhOS_priority_t prio)
{
  // Parameter checking:
  //
  if(task_handle == NULL)                                   return ERROR_INVALID_HANDLE;
  if((prio < PRIORITY_LOWEST) || (prio > PRIORITY_HIGHEST)) return ERROR_PARAM1;

  feabhOS_TASK task = *task_handle;
  vTaskPrioritySet(&task->handle, (portBASE_TYPE)prio);

  return ERROR_OK;
}


feabhOS_error feabhOS_task_destroy(feabhOS_TASK * const task_handle)
{
  // Parameter checking:
  //
  if(task_handle == NULL) return ERROR_INVALID_HANDLE;

  feabhOS_TASK task = *task_handle;
  vTaskDelete(task->handle);

  // Ensure client's handle is invalid
  //
  task->handle = NULL;

  return ERROR_OK;
}


feabhOS_error feabhOS_task_suspend(feabhOS_TASK * const task_handle)
{
  // Parameter checking:
  //
  if(task_handle == NULL) return ERROR_INVALID_HANDLE;

  feabhOS_TASK task = *task_handle;
  vTaskSuspend(task->handle);

  return ERROR_OK;
}


feabhOS_error feabhOS_task_resume(feabhOS_TASK * const task_handle)
{
  // Parameter checking:
  //
  if(task_handle == NULL) return ERROR_INVALID_HANDLE;

  feabhOS_TASK task = *task_handle;
  vTaskResume(task->handle);

  return ERROR_OK;
}


void feabhOS_task_sleep(duration_mSec_t period)
{
  portTickType delay = (OS_TIME_TYPE)(period) / portTICK_RATE_MS;
  vTaskDelay(delay);
}


void feabhOS_task_yield(void)
{
  feabhOS_task_sleep(0);
}



