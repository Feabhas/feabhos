// -------------------------------------------------------------------------------------
//  FeabhOS OS abstraction layer
//
//  DISCLAIMER:
//  Feabhas is furnishing this item "as is". Feabhas does not provide any warranty
//  of the item whatsoever, whether express, implied, or statutory, including, but
//  not limited to, any warranty of merchantability or fitness for a particular
//  purpose or any warranty that the contents of the item will be error-free.
//  In no respect shall Feabhas incur any liability for any damages, including, but
//  limited to, direct, indirect, special, or consequential damages arising out of,
//  resulting from, or any way connected to the use of the item, whether or not
//  based upon warranty, contract, tort, or otherwise; whether or not injury was
//  sustained by persons or property or otherwise; and whether or not loss was
//  sustained from, or arose out of, the results of, the item, or any services that
//  may be provided by Feabhas.
//
// -------------------------------------------------------------------------------------

#include "feabhOS_queue.h"
#include "feabhOS_defs.h"
#include <assert.h>

typedef struct feabhOS_queue
{
  OS_QUEUE_TYPE handle;
  
} feabhOS_queue_t;

// ----------------------------------------------------------------------------
//
//  MEMORY MANAGEMENT FOR QUEUE STRUCTURES
//  --------------------------------------
//
//  for a FreeRTOS implementation we don't
//  allow dynamic allocation of queue structs.
//  This is to keep control of heap memory in our
//  limited-resource system.
//  If the client has set MAX_QUEUES to NO_LIMIT
//  we need to stop compilation with an error.

#if MAX_QUEUES==NO_LIMIT

#error "You must not set MAX_QUEUES to NO_LIMIT with FreeRTOS"

#endif


//  Queue structure static allocation.
//  For simplicity, every time a queue is 'created'
//  by the client the next element in the array is
//  used; until they are all gone.
//  There is no re-use of queues in this implementation.
//
//  If all queues are taken the code will assert.
//
static feabhOS_QUEUE get_instance(void)
{
  static feabhOS_queue_t instances[MAX_QUEUES];
  static unsigned int next = 0;

  assert(next != MAX_QUEUES);

  return &instances[next++];
}


feabhOS_error feabhOS_queue_create(feabhOS_QUEUE * const queue_handle, size_bytes_t elemSize, num_elements_t queueSize)
{
  feabhOS_QUEUE queue = get_instance();

  queue->handle = xQueueCreate((portBASE_TYPE)queueSize, (portBASE_TYPE)elemSize);
  
  if(queue->handle != 0)
  {
    *queue_handle = queue;
    return ERROR_OK;
  }
  else
  {
    return ERROR_OUT_OF_MEMORY;
  }
}


feabhOS_error feabhOS_queue_put(feabhOS_QUEUE * const queue_handle, void * const in,  duration_mSec_t timeout)
{
  // Parameter checking:
  //
  if(queue_handle == NULL) return ERROR_INVALID_HANDLE;
  if(in == NULL)           return ERROR_PARAM1;

  feabhOS_QUEUE queue = *queue_handle;
  feabhOS_error err = ERROR_UNKNOWN;
  OS_ERROR_TYPE OSError;
  
  OSError = xQueueSendToBack(queue->handle, in, (OS_TIME_TYPE)timeout);
  if(OSError == pdPASS)
  {
    err = ERROR_OK;
  }
  else
  {
    /* Timeout expired; and still no space on the queue. */
    err = ERROR_QUEUE_FULL;
  }
  return err;
}


feabhOS_error feabhOS_queue_get(feabhOS_QUEUE * const queue_handle, void * const out, duration_mSec_t timeout)
{
  // Parameter checking:
  //
  if(queue_handle == NULL) return ERROR_INVALID_HANDLE;
  if(out == NULL)          return ERROR_PARAM1;

  feabhOS_QUEUE queue = *queue_handle;
  OS_ERROR_TYPE OSError;
  
  OSError = xQueueReceive(queue->handle, out, (OS_TIME_TYPE)timeout);

  if(OSError == pdPASS)
  {
     return ERROR_OK;
  }
  else
  {
    // Timeout expired; and still no items on the queue.
    //
    return ERROR_QUEUE_EMPTY;
  }
}


num_elements_t feabhOS_queue_size(feabhOS_QUEUE *queue_handle)
{
  // Parameter checking:
  // An invalid queue will always be empty!
  //
  if(queue_handle == NULL) return 0;

  feabhOS_QUEUE queue = *queue_handle;
  return (num_elements_t)uxQueueMessagesWaiting(queue->handle);
}


feabhOS_error feabhOS_queue_destroy(feabhOS_QUEUE *queue_handle)
{
  // Parameter checking:
  //
  if(queue_handle == NULL) return ERROR_INVALID_HANDLE;

  feabhOS_QUEUE queue = *queue_handle;
  vQueueDelete(queue->handle);

  // Ensure the client's handle is invalid
  //
  queue->handle = NULL;

  return ERROR_OK;
}

