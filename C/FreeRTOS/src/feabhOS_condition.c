// -------------------------------------------------------------------------------------
//  FeabhOS OS abstraction layer
//
//  DISCLAIMER:
//  Feabhas is furnishing this item "as is". Feabhas does not provide any warranty
//  of the item whatsoever, whether express, implied, or statutory, including, but
//  not limited to, any warranty of merchantability or fitness for a particular
//  purpose or any warranty that the contents of the item will be error-free.
//  In no respect shall Feabhas incur any liability for any damages, including, but
//  limited to, direct, indirect, special, or consequential damages arising out of,
//  resulting from, or any way connected to the use of the item, whether or not
//  based upon warranty, contract, tort, or otherwise; whether or not injury was
//  sustained by persons or property or otherwise; and whether or not loss was
//  sustained from, or arose out of, the results of, the item, or any services that
//  may be provided by Feabhas.
//
// -------------------------------------------------------------------------------------

#include "feabhOS_condition.h"
#include "feabhOS_defs.h"
#include "feabhOS_signal.h"
#include "feabhOS_mutex.h"
#include <assert.h>

typedef struct feabhOS_condition
{
  feabhOS_SIGNAL signal;
  
} feabhOS_condition_t;


// ----------------------------------------------------------------------------
//
//  MEMORY MANAGEMENT FOR CONDITION STRUCTURES
//  ------------------------------------------
//
//  for a FreeRTOS implementation we don't
//  allow dynamic allocation of condition structs.
//  This is to keep control of heap memory in our
//  limited-resource system.
//  If the client has set MAX_CONDITIONS to NO_LIMIT
//  we need to stop compilation with an error.

#if MAX_MUTEXES==NO_LIMIT

#error "You must not set MAX_CONDITIONS to NO_LIMIT with FreeRTOS"

#endif


//  Condition structure static allocation.
//  For simplicity, every time a condition is 'created'
//  by the client the next element in the array is
//  used; until they are all gone.
//  There is no re-use of conditions in this implementation.
//
//  If all conditions are taken the code will assert.
//
static feabhOS_CONDITION get_instance(void)
{
  static feabhOS_condition_t instances[MAX_CONDITIONS];
  static unsigned int next = 0;

  assert(next != MAX_CONDITIONS);

  return &instances[next++];
}


feabhOS_error feabhOS_condition_create(feabhOS_CONDITION * const condition_handle)
{
  feabhOS_CONDITION condition;
  feabhOS_error err = ERROR_UNKNOWN;

  condition = get_instance();
  
  if(condition != NULL)
  {
    err = feabhOS_signal_create(&condition->signal);
  }
  else
  {
    err = ERROR_OUT_OF_MEMORY;
  }

  *condition_handle = condition;

  return err;
}


feabhOS_error feabhOS_condition_notify(feabhOS_CONDITION * const condition_handle)
{
  // Parameter checking:
  //
  if(condition_handle == NULL) return ERROR_INVALID_HANDLE;

  feabhOS_CONDITION condition = *condition_handle;
  feabhOS_signal_notify_all(&condition->signal);

  return ERROR_OK;
}


feabhOS_error feabhOS_condition_wait(feabhOS_CONDITION *condition_handle, feabhOS_MUTEX * const mutex_handle, duration_mSec_t timeout)
{
  // Parameter checking:
  //
  if(condition_handle == NULL) return ERROR_INVALID_HANDLE;
  if(mutex_handle == NULL)     return ERROR_PARAM1;

  feabhOS_CONDITION condition = *condition_handle;
  feabhOS_error err;
  
  feabhOS_mutex_unlock(mutex_handle);
  err = feabhOS_signal_wait(&condition->signal, timeout);
  feabhOS_mutex_lock(mutex_handle, WAIT_FOREVER);

  return err;
}


feabhOS_error feabhOS_condition_destroy(feabhOS_CONDITION * const condition_handle)
{
  // Parameter checking:
  //
  if(condition_handle == NULL) return ERROR_INVALID_HANDLE;

  feabhOS_CONDITION condition = *condition_handle;
  feabhOS_signal_destroy(&condition->signal);

  // Ensure client's handle is invalid
  //
  condition->signal = NULL;

  return ERROR_OK;
}
