// -------------------------------------------------------------------------------------
//  FeabhOS OS abstraction layer
//
//  DISCLAIMER:
//  Feabhas is furnishing this item "as is". Feabhas does not provide any warranty
//  of the item whatsoever, whether express, implied, or statutory, including, but
//  not limited to, any warranty of merchantability or fitness for a particular
//  purpose or any warranty that the contents of the item will be error-free.
//  In no respect shall Feabhas incur any liability for any damages, including, but
//  limited to, direct, indirect, special, or consequential damages arising out of,
//  resulting from, or any way connected to the use of the item, whether or not
//  based upon warranty, contract, tort, or otherwise; whether or not injury was
//  sustained by persons or property or otherwise; and whether or not loss was
//  sustained from, or arose out of, the results of, the item, or any services that
//  may be provided by Feabhas.
//
// -------------------------------------------------------------------------------------

#include "feabhOS_mutex.h"
#include "feabhOS_defs.h"
#include <assert.h>

typedef struct feabhOS_mutex
{
  OS_MUTEX_TYPE handle;
  
} feabhOS_mutex_t;

// ----------------------------------------------------------------------------
//
//  MEMORY MANAGEMENT FOR MUTEX STRUCTURES
//  --------------------------------------
//
//  for a FreeRTOS implementation we don't
//  allow dynamic allocation of mutex structs.
//  This is to keep control of heap memory in our
//  limited-resource system.
//  If the client has set MAX_MUTEXES to NO_LIMIT
//  we need to stop compilation with an error.

#if MAX_MUTEXES==NO_LIMIT

#error "You must not set MAX_MUTEXES to NO_LIMIT with FreeRTOS"

#endif


//  Mutex structure static allocation.
//  For simplicity, every time a mutex is 'created'
//  by the client the next element in the array is
//  used; until they are all gone.
//  There is no re-use of mutexes in this implementation.
//
//  If all mutexes are taken the code will assert.
//
static feabhOS_MUTEX get_instance(void)
{
  static feabhOS_mutex_t instances[MAX_MUTEXES];
  static unsigned int next = 0;

  assert(next != MAX_MUTEXES);

  return &instances[next++];
}



feabhOS_error feabhOS_mutex_create(feabhOS_MUTEX * const mutex_handle)
{
  feabhOS_MUTEX mutex = get_instance();

  mutex->handle = xSemaphoreCreateMutex();
  
  if(mutex->handle != 0)
  {
    *mutex_handle = mutex;
    return ERROR_OK;
  }
  else
  {
    return ERROR_OUT_OF_MEMORY;
  }
}


feabhOS_error feabhOS_mutex_lock(feabhOS_MUTEX * const mutex_handle, duration_mSec_t timeout)
{
  // Parameter checking:
  //
  if(mutex_handle == NULL) return ERROR_INVALID_HANDLE;

  feabhOS_MUTEX queue = *mutex_handle;
  OS_ERROR_TYPE OSError;

  OSError = xSemaphoreTake(queue->handle, (OS_TIME_TYPE)timeout);

  if (OSError == pdTRUE) return ERROR_OK;
  else                   return ERROR_TIMED_OUT;
}


feabhOS_error feabhOS_mutex_unlock(feabhOS_MUTEX * const mutex_handle)
{
  // Parameter checking:
  //
  if(mutex_handle == NULL) return ERROR_INVALID_HANDLE;

  feabhOS_MUTEX queue = *mutex_handle;
  xSemaphoreGive(queue->handle);

  return ERROR_OK;
}


feabhOS_error feabhOS_mutex_destroy(feabhOS_MUTEX * const mutex_handle)
{
  // No clean-up in FreeRTOS.

  // Ensure client's handle is invalid
  //
  feabhOS_MUTEX queue = *mutex_handle;
  queue->handle = NULL;

  return ERROR_OK;
}
