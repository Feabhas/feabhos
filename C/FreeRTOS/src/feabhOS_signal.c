// -------------------------------------------------------------------------------------
//  FeabhOS OS abstraction layer
//
//  DISCLAIMER:
//  Feabhas is furnishing this item "as is". Feabhas does not provide any warranty
//  of the item whatsoever, whether express, implied, or statutory, including, but
//  not limited to, any warranty of merchantability or fitness for a particular
//  purpose or any warranty that the contents of the item will be error-free.
//  In no respect shall Feabhas incur any liability for any damages, including, but
//  limited to, direct, indirect, special, or consequential damages arising out of,
//  resulting from, or any way connected to the use of the item, whether or not
//  based upon warranty, contract, tort, or otherwise; whether or not injury was
//  sustained by persons or property or otherwise; and whether or not loss was
//  sustained from, or arose out of, the results of, the item, or any services that
//  may be provided by Feabhas.
//
// -------------------------------------------------------------------------------------
#include "feabhOS_signal.h"
#include "feabhOS_semaphore.h"
#include <assert.h>

/*---------------------------------------*/
/* 
  NOTE:
  EmbOS does not support the idea of a 
  task-independent signal, so we must 
  simulate it with a binary semaphore
  (The 'semaphore-as-signal' pattern)
*/
/*---------------------------------------*/

typedef struct feabhOS_signal
{
  feabhOS_SEMAPHORE sem_handle;
  num_elements_t    waitingTasks;
  
} feabhOS_signal_t;

// ----------------------------------------------------------------------------
//
//  MEMORY MANAGEMENT FOR SIGNAL STRUCTURES
//  --------------------------------------
//
//  for a FreeRTOS implementation we don't
//  allow dynamic allocation of signal structs.
//  This is to keep control of heap memory in our
//  limited-resource system.
//  If the client has set MAX_SIGNALS to NO_LIMIT
//  we need to stop compilation with an error.

#if MAX_SIGNALS==NO_LIMIT

#error "You must not set MAX_SIGNALS to NO_LIMIT with FreeRTOS"

#endif


//  Signal structure static allocation.
//  For simplicity, every time a signal is 'created'
//  by the client the next element in the array is
//  used; until they are all gone.
//  There is no re-use of signals in this implementation.
//
//  If all signals are taken the code will assert.
//
static feabhOS_SIGNAL get_instance(void)
{
  static feabhOS_signal_t instances[MAX_SIGNALS];
  static unsigned int next = 0;

  assert(next != MAX_SIGNALS);

  return &instances[next++];
}


feabhOS_error feabhOS_signal_create(feabhOS_SIGNAL * const signal_handle)
{
  feabhOS_SIGNAL signal = get_instance();
  feabhOS_error  err;

  err = feabhOS_semaphore_create(&signal->sem_handle);
  if(err == ERROR_OUT_OF_MEMORY) return ERROR_OUT_OF_MEMORY;
  
  // Put the semaphore in the 'taken' state.  If this
  // fails something very odd has happened.
  //
  err = feabhOS_semaphore_take(&signal->sem_handle, WAIT_FOREVER);
  if(err != ERROR_OK) return ERROR_UNKNOWN;

  signal->waitingTasks = 0;

  *signal_handle = signal;
  return ERROR_OK;
}


feabhOS_error feabhOS_signal_notify_one(feabhOS_SIGNAL * const signal_handle)
{
  // Parameter checking:
  //
  if(signal_handle == NULL) return ERROR_INVALID_HANDLE;

  feabhOS_SIGNAL signal = *signal_handle;

  // Give the semaphore, releasing any waiting tasks
  //
  feabhOS_semaphore_give(&signal->sem_handle);
  if(signal->waitingTasks > 0)
  {
    signal->waitingTasks--;
  }

  return ERROR_OK;
}

feabhOS_error feabhOS_signal_notify_all(feabhOS_SIGNAL * const signal_handle)
{
  // Parameter checking:
  //
  if(signal_handle == NULL) return ERROR_INVALID_HANDLE;

  feabhOS_SIGNAL signal = *signal_handle;

  /* Give the semaphore, releasing any waiting tasks */
  while(signal->waitingTasks > 0)
  {
    feabhOS_semaphore_give(&signal->sem_handle);
    signal->waitingTasks--;
  }

  return ERROR_OK;
}


feabhOS_error feabhOS_signal_wait(feabhOS_SIGNAL * const signal_handle, duration_mSec_t timeout)
{
  // Parameter checking:
  //
  if(signal_handle == NULL) return ERROR_INVALID_HANDLE;

  feabhOS_SIGNAL signal = *signal_handle;
  (void) timeout;
  feabhOS_error err;

  // Attempt to take the semaphore, which should pend
  //
  signal->waitingTasks++;
  err = feabhOS_semaphore_take(&signal->sem_handle, WAIT_FOREVER);
  
  return err;
}


feabhOS_error feabhOS_signal_destroy(feabhOS_SIGNAL * const signal_handle)
{
  // Parameter checking:
  //
  if(signal_handle == NULL) return ERROR_INVALID_HANDLE;

  feabhOS_SIGNAL signal = *signal_handle;

  // No cleanup in FreeRTOS.

  // Ensure the client's handle is invalid
  //
  signal->sem_handle = NULL;
  signal->waitingTasks = 0;

  return ERROR_OK;
}
