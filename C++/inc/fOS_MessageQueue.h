// -------------------------------------------------------------------------------------
//  FeabhOS OS abstraction layer
//
//  DISCLAIMER:
//  Feabhas is furnishing this item "as is". Feabhas does not provide any warranty
//  of the item whatsoever, whether express, implied, or statutory, including, but
//  not limited to, any warranty of merchantability or fitness for a particular
//  purpose or any warranty that the contents of the item will be error-free.
//  In no respect shall Feabhas incur any liability for any damages, including, but
//  limited to, direct, indirect, special, or consequential damages arising out of,
//  resulting from, or any way connected to the use of the item, whether or not
//  based upon warranty, contract, tort, or otherwise; whether or not injury was
//  sustained by persons or property or otherwise; and whether or not loss was
//  sustained from, or arose out of, the results of, the item, or any services that
//  may be provided by Feabhas.
//
// -------------------------------------------------------------------------------------
#ifndef MESSAGEQUEUE_H
#define MESSAGEQUEUE_H

#include "fOS_TemplateBuffer.h"
#include "fOS_Mutex.h"
#include "fOS_Condition.h"
#include "fOS_Time.h"
#include <cstddef>

namespace FeabhOS
{
  template<typename Message_Ty = int, unsigned int sz = 8>
  class MessageQueue : private Utilities::Buffer<Message_Ty, sz>
  {
  public:
    MessageQueue() = default;

    // Post/Get API
    // MessageQueue supports both moving and copying of elements
    // into the underlying container.
    //
    // post()         - Blocking call; will block forever to
    //                  post a message.
    // try_post()     - Non-blocking; will return false if message
    //                  cannot be posted
    // try_post_for() - Blocking call; will attempt to post until
    //                  timeout expires
    //
    template <typename T> void post(T&& in_msg);
    template <typename T> bool try_post(T&& in_msg);
    template <typename T> bool try_post_for(T&& in_msg, const Time::Duration& timeout);

    void get(Message_Ty& inout_msg);
    bool try_get(Message_Ty& inout_msg);
    bool try_get_for(Message_Ty& inout_msg, const Time::Duration& timeout);

    bool          is_empty();
    std::size_t   size();
    std::size_t   capacity();

    MessageQueue(const MessageQueue&)            = delete;
    MessageQueue& operator=(const MessageQueue&) = delete;
    MessageQueue(MessageQueue&&)                 = delete;
    MessageQueue& operator=(MessageQueue&&)      = delete;

private:
    using Underlying_Ty =  Utilities::Buffer<Message_Ty, sz>;

    Mutex mutex;
    Condition has_data;
    Condition has_space;
  };


  template<typename Message_Ty, unsigned int sz>
  template <typename T>
  void
  MessageQueue<Message_Ty, sz>::post(T&& in_msg)
  {
    return try_post_for(std::forward<T>(in_msg), Time::wait_forever);
  }


  template<typename Message_Ty, unsigned int sz>
  template <typename T>
  bool
  MessageQueue<Message_Ty, sz>::try_post(T&& in_msg)
  {
    return try_post_for(std::forward<T>(in_msg), Time::no_wait);
  }


  template<typename Message_Ty, unsigned int sz>
  template <typename T>
  bool
  MessageQueue<Message_Ty, sz>::try_post_for(T&& in_msg, const Time::Duration& timeout)
  {
    CRITICAL_SECTION(mutex)
    {
      // NOTE: Here we must make a direct call
      // to the base class size() function,
      // rather than call our public method.  If
      // we call the public method we will deadlock
      // the system because we've already locked
      // the mutex!
      //
      while(Underlying_Ty::size() == sz)
      {
        if(!has_space.wait_for(mutex, timeout)) return false;
      }
      Underlying_Ty::add(std::forward<T>(in_msg));
      has_data.notify_all();
    }
    return true;
  }


  template<typename Message_Ty, unsigned int sz>
  void
  MessageQueue<Message_Ty, sz>::get(Message_Ty& inout_msg)
  {
    try_get_for(inout_msg, Time::wait_forever);
  }


  template<typename Message_Ty, unsigned int sz>
  bool
  MessageQueue<Message_Ty, sz>::try_get(Message_Ty& inout_msg)
  {
    return try_get_for(inout_msg, Time::no_wait);
  }


  template<typename Message_Ty, unsigned int sz>
  bool
  MessageQueue<Message_Ty, sz>::try_get_for(Message_Ty& inout_msg, const Time::Duration& timeout)
  {
    CRITICAL_SECTION(mutex)
    {
      // NOTE: Here we must make a direct call
      // to the base class is_empty() function,
      // rather than call our public method.  If
      // we call the public method we will deadlock
      // the system because we've already locked
      // the mutex!
      //
      while(Underlying_Ty::is_empty())
      {
        if(!has_data.wait_for(mutex, timeout)) return false;
      }
      Underlying_Ty::get(inout_msg);
      has_space.notify_all();
    }
    return true;
  }


  template<typename Message_Ty, unsigned int sz>
  bool
  MessageQueue<Message_Ty, sz>::is_empty()
  {
    bool empty { };
    CRITICAL_SECTION(mutex)
    {
      empty = Underlying_Ty::is_empty();
    }
    return empty;
  }


  template<typename Message_Ty, unsigned int sz>
  std::size_t
  MessageQueue<Message_Ty, sz>::size()
  {
    std::size_t size { };
    CRITICAL_SECTION(mutex)
    {
      size = Underlying_Ty::size();
    }
    return size;
  }


  template<typename Message_Ty, unsigned int sz>
  std::size_t
  MessageQueue<Message_Ty, sz>::capacity()
  {
    return sz;
  }

} // namespace FeabhOS



#endif // MESSAGEQUEUE_H
