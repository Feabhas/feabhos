// -------------------------------------------------------------------------------------
//  FeabhOS OS abstraction layer
//
//  DISCLAIMER:
//  Feabhas is furnishing this item "as is". Feabhas does not provide any warranty
//  of the item whatsoever, whether express, implied, or statutory, including, but
//  not limited to, any warranty of merchantability or fitness for a particular
//  purpose or any warranty that the contents of the item will be error-free.
//  In no respect shall Feabhas incur any liability for any damages, including, but
//  limited to, direct, indirect, special, or consequential damages arising out of,
//  resulting from, or any way connected to the use of the item, whether or not
//  based upon warranty, contract, tort, or otherwise; whether or not injury was
//  sustained by persons or property or otherwise; and whether or not loss was
//  sustained from, or arose out of, the results of, the item, or any services that
//  may be provided by Feabhas.
//
// -------------------------------------------------------------------------------------

#ifndef THREAD_H
#define THREAD_H

#include "feabhOS_task.h"
#include "fOS_Time.h"
#include "fOS_Callback.h"
#include <stdexcept>
#include <memory>
#include <cassert>

namespace FeabhOS
{
  // ------------------------------------------------------------------------------
  // Thread exceptions
  //
  class ThreadCreationFailed : public std::runtime_error
  {
  public:
    ThreadCreationFailed(const char* str) : std::runtime_error(str) {}
    ThreadCreationFailed() : std::runtime_error("Thread creation failed!") {}
  };


  class ThreadAlreadyCreated : public std::runtime_error
  {
  public:
    ThreadAlreadyCreated(const char* str) : std::runtime_error(str) {}
    ThreadAlreadyCreated() : std::runtime_error("Thread already running!") {}
  };

  // ------------------------------------------------------------------------------
  // Thread stack and priority definitions
  //
  enum class Priority
  {
    Lowest  = PRIORITY_LOWEST,
    Low     = PRIORITY_LOW,
    Normal  = PRIORITY_NORMAL,
    High    = PRIORITY_HIGH,
    Highest = PRIORITY_HIGHEST
  };

  enum Stack
  {
    Tiny    = STACK_TINY,
    Small   = STACK_SMALL,
    Normal  = STACK_NORMAL,
    Large   = STACK_LARGE,
    Huge    = STACK_HUGE
  };


  /// ------------------------------------------------------------------------------
  // Thread manages the underlying OS thread-of-control
  //
  class Thread
  {
  public:
    // -----------------------------------------------------------------------------
    // Thread construction.
    // There are three basic options:
    // - If you are happy with the default priority and stack size
    //   you can construct a Thread with the behaviour it will run.
    //   Attempting to call attach() after this will fail.
    //
    // - Construct a thread, adjusting stack/priority as required.
    //   Then call attach() to add the behaviour.
    //
    // - Constructing an empty Thread and attaching no behaviour
    //   will do nothing.
    //
    Thread()                               = default;
    Thread(Priority prio)                  : priority { prio } {}
    Thread(Stack stacksize)                : stack { stacksize } {};
    Thread(Priority prio, Stack stacksize) : priority { prio }, stack { stacksize } {}

    template <typename Fn_Ty, typename... Param_Ty>
    inline
    Thread(Fn_Ty fn, Param_Ty&&... arg);

    // -----------------------------------------------------------------------------
    // Thread destruction
    // Destroying a Thread object will cause it to delete the
    // underlying OS thread.  The behaviour of your application
    // is undefined.
    //
    ~Thread();

    // -----------------------------------------------------------------------------
    // Thread behaviour
    // Calling attach() associates this Thread object with a callable
    // object (function).  In this implementation you can only
    // attach behaviour once.
    //
    template <typename Fn_Ty, typename... Param_Ty>
    inline
    void attach(Fn_Ty fn, Param_Ty&&... arg);

    // -----------------------------------------------------------------------------
    // Thread management.
    // - Adjusting Thread priority can be done at any time.
    //
    // - Adjusting stack size can only be done while no behavoiur
    //   is attached to this thread.  Attempting to change stack
    //   size after this will return an 'invalid call' error.
    //
    // - Calling suspend() / resume() on a Thread with no
    //   behaviour attached has no effect.
    //
    feabhOS_error set_stack(Stack stck);
    Priority      get_priority();
    void          set_priority(Priority prio);
    void          suspend();
    void          resume();

    // -----------------------------------------------------------------------------
    // Thread blocking calls
    // sleep() and yield() always affect the calling Thread object
    //
    static void sleep(Time::Duration period);
    static void yield();

    // -----------------------------------------------------------------------------
    // Thread copy and move policy.
    // Threads support moving but not copying.
    //
    Thread(const Thread&)             = delete;
    Thread& operator= (const Thread&) = delete;
    Thread(Thread&&)                  = default;
    Thread& operator= (Thread&&)      = default;

  private:
    // -----------------------------------------------------------------------------
    // Thread management data
    //
    feabhOS_TASK handle   { nullptr };
    Priority     priority { Priority::Normal };
    Stack        stack    { Stack::Normal };

    // -----------------------------------------------------------------------------
    // This function is supplied to the C API.  It is the function registered with
    // the underlying OS.
    // FeabhOS does not support run-to-completion semantics on its tasks, so if the
    // client function exits it will be restarted.  If the client function terminates
    // with an exception the code will assert.
    //
    template <typename Callback_Ty>
    static void scheduled_function(void* arg);

    // -----------------------------------------------------------------------------
    // Create the underlying OS task.
    // Implementation details below.
    //
    template <typename Fn_Ty, typename... Param_Ty>
    typename std::enable_if<!std::is_member_function_pointer<Fn_Ty>::value, void>::type
    create_OS_task(Fn_Ty fn, Param_Ty... arg);

    template <typename Fn_Ty, typename... Param_Ty>
    typename std::enable_if<std::is_member_function_pointer<Fn_Ty>::value, void>::type
    create_OS_task(Fn_Ty fn, Param_Ty... arg);
  };


  // -----------------------------------------------------------------------------
  // This function is supplied to the C API.  It is the function registered with
  // the underlying OS.  Note, even though it is a template function it has the
  // same call signature as a normal C function.
  // The scheduled_function() is responsible for managing the lifetime of the
  // RunPolicy object and also the Callback object. The lifetimes of these objects
  // will be the lifetime of the OS task/thread, since exiting this function will
  // terminate the OS task/thread.
  //
  template <typename Callback_Ty>
  void Thread::scheduled_function(void* arg)
  {
    std::unique_ptr<Callback_Ty> callback_ptr { reinterpret_cast<Callback_Ty*>(arg) };
    try
    {
      while(true) (*callback_ptr)();
    }
    catch(...)
    {
      assert(false);
    }
  }

  // -----------------------------------------------------------------------------
  // The create_OS_task() function invokes the underlying OS (via a C API).  The
  // function creates an appropriate Callback object deduced from its parameters.
  // The Callback object must be dynamically created since its lifetime will
  // be longer than the create_OS_task() function. Notice that the lifetime of the
  // Callback object is not managed by this function; that responsibility is handed
  // over to the scheduled_function() (which manages it using a unique_ptr).
  //
  // The create_OS_task() function is overloaded (with the same API!) for normal
  // (free) functions and class-member functions.  std::enable_if is used to disable
  // invalid overloads (using SFINAE).
  //
  // If the thread can't be created for some reason (most likely not enough memory)
  // then tidy up  the Callback object and inform the client by throwing an exception.
  // Why an exception?
  // - We are using enable_if to provide different implementations for member and non-member
  //   functions.
  // - We can't use the template parameter list for enable_if as we have a default
  //   template parameter
  // - We can't use a default argument as we are using a variadic template argument list
  // - We must therefore use the return type for enable_if
  // - Thus there's no other way of returning an error!
  //
  template <typename Fn_Ty, typename... Param_Ty>
  typename std::enable_if<!std::is_member_function_pointer<Fn_Ty>::value, void>::type
  Thread::create_OS_task(Fn_Ty fn, Param_Ty... arg)
  {
    // Check to see if start has already been called on this thread
    // object.  Throw an exception if this is the case -
    // otherwise, the original OS thread would be leaked!
    //
    if(handle != nullptr) throw ThreadAlreadyCreated { };

    // Alias to simplify code
    //
    using Callback_Ty = FeabhOS::Utilities::Callback<Fn_Ty, Param_Ty...>;

    Callback_Ty* callback_ptr = new Callback_Ty { fn, std::forward<Param_Ty>(arg)... };

    feabhOS_error error = feabhOS_task_create(&handle,
                                              reinterpret_cast<void(*)(void*)>(&Thread::scheduled_function<Callback_Ty>),
                                              reinterpret_cast<void*>(callback_ptr),
                                              static_cast<feabhOS_stack_size_t>(stack),
                                              static_cast<feabhOS_priority_t>(priority));

    if(error != ERROR_OK)
    {
      delete callback_ptr;
      throw ThreadCreationFailed { };
    }
  }


  template <typename Fn_Ty, typename... Param_Ty>
  typename std::enable_if<std::is_member_function_pointer<Fn_Ty>::value, void>::type
  Thread::create_OS_task(Fn_Ty fn, Param_Ty... arg)
  {
    if(handle != nullptr) throw ThreadAlreadyCreated { };

    // Note: Use std::mem_fn to convert the member function into a
    // functor.
    //
    using Callback_Ty = FeabhOS::Utilities::Callback<decltype(std::mem_fn(fn)), Param_Ty...>;

    Callback_Ty* callback_ptr = new Callback_Ty { std::mem_fn(fn), std::forward<Param_Ty>(arg)... };

    feabhOS_error error = feabhOS_task_create(&handle,
                                              reinterpret_cast<void(*)(void*)>(&Thread::scheduled_function<Callback_Ty>),
                                              reinterpret_cast<void*>(callback_ptr),
                                              static_cast<feabhOS_stack_size_t>(stack),
                                              static_cast<feabhOS_priority_t>(priority));

    if(error != ERROR_OK)
    {
      delete callback_ptr;
      throw ThreadCreationFailed { };
    }
  }


  template <typename Fn_Ty, typename... Param_Ty>
  Thread::Thread(Fn_Ty fn, Param_Ty&&... arg)
  {
    create_OS_task(fn, std::forward<Param_Ty>(arg)...);
  }


  template <typename Fn_Ty, typename... Param_Ty>
  void Thread::attach(Fn_Ty fn, Param_Ty&&... arg)
  {
    create_OS_task(fn, std::forward<Param_Ty>(arg)...);
  }

} // namespace FeabhOS



#endif //THREAD_H
