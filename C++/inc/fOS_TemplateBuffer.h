// -------------------------------------------------------------------------------------
//  FeabhOS OS abstraction layer
//
//  DISCLAIMER:
//  Feabhas is furnishing this item "as is". Feabhas does not provide any warranty
//  of the item whatsoever, whether express, implied, or statutory, including, but
//  not limited to, any warranty of merchantability or fitness for a particular
//  purpose or any warranty that the contents of the item will be error-free.
//  In no respect shall Feabhas incur any liability for any damages, including, but
//  limited to, direct, indirect, special, or consequential damages arising out of,
//  resulting from, or any way connected to the use of the item, whether or not
//  based upon warranty, contract, tort, or otherwise; whether or not injury was
//  sustained by persons or property or otherwise; and whether or not loss was
//  sustained from, or arose out of, the results of, the item, or any services that
//  may be provided by Feabhas.
//
// -------------------------------------------------------------------------------------
#ifndef TEMPLATEBUFFER_H
#define TEMPLATEBUFFER_H

#include <cstddef>

namespace FeabhOS
{
  namespace Utilities
  {
    template <typename T = int, std::size_t sz = 8>
    class Buffer
    {
    public:
      enum Error { OK, FULL, EMPTY };

      template <typename U>
      Error add(U&& in_val);

      Error get(T& inout_val);
      void  flush();

      bool        is_empty() const { return (num_items == 0); }
      std::size_t size()     const { return num_items; }
      std::size_t capacity() const { return sz; }

    private:
      unsigned int read      { 0 };
      unsigned int write     { 0 };
      unsigned int num_items { 0 };

      T buffer[sz];
    };


    template <typename T, std::size_t sz>
    template <typename U>
    typename Buffer<T, sz>::Error
    Buffer<T, sz>::add(U&& in_val)
    {
      if(num_items == sz) return FULL;

      buffer[write] = std::forward<U>(in_val);
      ++num_items;
      ++write;
      if(write == sz) write = 0;

      return OK;
    }


    template <typename T, size_t sz>
    typename Buffer<T, sz>::Error Buffer<T, sz>::get(T& inout_val)
    {
      if(num_items == 0) return EMPTY;

      inout_val = std::move(buffer[read]);
      --num_items;
      ++read;
      if(read == sz) read = 0;

      return OK;
    }


    template <typename T, size_t sz>
    void
    Buffer<T, sz>::flush()
    {
      read = 0;
      write = 0;
      num_items = 0;
    }

  }  // namespace Utilities

} // namespace FeabhOS

#endif // TEMPLATEBUFFER_H
