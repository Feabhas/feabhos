// -------------------------------------------------------------------------------------
//  FeabhOS OS abstraction layer
//
//  DISCLAIMER:
//  Feabhas is furnishing this item "as is". Feabhas does not provide any warranty
//  of the item whatsoever, whether express, implied, or statutory, including, but
//  not limited to, any warranty of merchantability or fitness for a particular
//  purpose or any warranty that the contents of the item will be error-free.
//  In no respect shall Feabhas incur any liability for any damages, including, but
//  limited to, direct, indirect, special, or consequential damages arising out of,
//  resulting from, or any way connected to the use of the item, whether or not
//  based upon warranty, contract, tort, or otherwise; whether or not injury was
//  sustained by persons or property or otherwise; and whether or not loss was
//  sustained from, or arose out of, the results of, the item, or any services that
//  may be provided by Feabhas.
//
// -------------------------------------------------------------------------------------
#ifndef CONDITION_H
#define CONDITION_H

#include "fOS_Signal.h"

namespace FeabhOS
{
  namespace Time { class Duration; }
  class Mutex;

  class Condition
  {
  public:
    Condition() = default;
    ~Condition() = default;

    // Condition API:
    // wait()     - Blocking call; will block forever for
    //              condition to be signalled.
    // wait_for() - Blocking call; will wait for condition
    //              until timeout expires
    //
    void wait(Mutex& mutex);
    bool wait_for(Mutex& mutex, const Time::Duration& timeout);

    // notify_one() - Release one thread.
    // notify_all() - Release all waiting threads
    //
    void notify_one();
    void notify_all();

    Condition(const Condition&)            = delete;
    Condition& operator=(const Condition&) = delete;
    Condition(Condition&&)                 = delete;
    Condition& operator=(Condition&&)      = delete;

  private:
    Signal signal{};
  };

} // namespace FeabhOS

#endif // CONDITION_H
