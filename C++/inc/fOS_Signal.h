// -------------------------------------------------------------------------------------
//  FeabhOS OS abstraction layer
//
//  DISCLAIMER:
//  Feabhas is furnishing this item "as is". Feabhas does not provide any warranty
//  of the item whatsoever, whether express, implied, or statutory, including, but
//  not limited to, any warranty of merchantability or fitness for a particular
//  purpose or any warranty that the contents of the item will be error-free.
//  In no respect shall Feabhas incur any liability for any damages, including, but
//  limited to, direct, indirect, special, or consequential damages arising out of,
//  resulting from, or any way connected to the use of the item, whether or not
//  based upon warranty, contract, tort, or otherwise; whether or not injury was
//  sustained by persons or property or otherwise; and whether or not loss was
//  sustained from, or arose out of, the results of, the item, or any services that
//  may be provided by Feabhas.
//
// -------------------------------------------------------------------------------------
#ifndef SIGNAL_H
#define SIGNAL_H

#include "feabhOS_signal.h"

namespace FeabhOS
{
  namespace Time { class Duration; }

  class Signal
  {
  public:
    Signal();
    ~Signal();

    // Signal API:
    // wait()     - Blocking call; will block forever for
    //              signal to be signalled.
    // wait_for() - Blocking call; will wait for signal
    //              until timeout expires
    //
    void wait();
    bool wait_for(const Time::Duration& timeout);

    // notify_one() - Release one thread.
    // notify_all() - Release all waiting threads
    //
    void notify_one();
    void notify_all();
  
    Signal(const Signal&)            = delete;
    Signal& operator=(const Signal&) = delete;
    Signal(Signal&&)                 = delete;
    Signal& operator=(Signal&&)      = delete;

  private:
    feabhOS_SIGNAL handle { nullptr };
  };

} // namespace FeabhOS

#endif // SIGNAL_H
