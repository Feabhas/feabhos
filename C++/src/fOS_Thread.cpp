// -------------------------------------------------------------------------------------
//  FeabhOS OS abstraction layer
//
//  DISCLAIMER:
//  Feabhas is furnishing this item "as is". Feabhas does not provide any warranty
//  of the item whatsoever, whether express, implied, or statutory, including, but
//  not limited to, any warranty of merchantability or fitness for a particular
//  purpose or any warranty that the contents of the item will be error-free.
//  In no respect shall Feabhas incur any liability for any damages, including, but
//  limited to, direct, indirect, special, or consequential damages arising out of,
//  resulting from, or any way connected to the use of the item, whether or not
//  based upon warranty, contract, tort, or otherwise; whether or not injury was
//  sustained by persons or property or otherwise; and whether or not loss was
//  sustained from, or arose out of, the results of, the item, or any services that
//  may be provided by Feabhas.
//
// -------------------------------------------------------------------------------------
#include "fOS_Thread.h"

namespace FeabhOS
{
  Thread::~Thread()
  {
    if(handle != nullptr) feabhOS_task_destroy(&handle);
  }


  feabhOS_error Thread::set_stack(Stack stck)
  {
    if(handle != nullptr) return ERROR_STUPID;

    stack = stck;
    return ERROR_OK;
  }


  Priority Thread::get_priority()
  {
    return priority;
  }


  void Thread::set_priority(Priority prio)
  {
    if(handle == nullptr) // OS thread not created.
    {
      priority = prio;
    }
    else                  // OS thread created
    {
      feabhOS_task_setPriority(&handle, static_cast<feabhOS_priority_t>(prio));
    }
  }


  void Thread::suspend()
  {
    if(handle != nullptr) feabhOS_task_suspend(&handle);
  }


  void Thread::resume()
  {
    if(handle != nullptr) feabhOS_task_suspend(&handle);
  }


  void Thread::sleep(Time::Duration period)
  {
    feabhOS_task_sleep(period);
  }


  void Thread::yield()
  {
    feabhOS_task_yield();
  }



} // namespace FeabhOS
