// -------------------------------------------------------------------------------------
//  FeabhOS OS abstraction layer
//
//  DISCLAIMER:
//  Feabhas is furnishing this item "as is". Feabhas does not provide any warranty
//  of the item whatsoever, whether express, implied, or statutory, including, but
//  not limited to, any warranty of merchantability or fitness for a particular
//  purpose or any warranty that the contents of the item will be error-free.
//  In no respect shall Feabhas incur any liability for any damages, including, but
//  limited to, direct, indirect, special, or consequential damages arising out of,
//  resulting from, or any way connected to the use of the item, whether or not
//  based upon warranty, contract, tort, or otherwise; whether or not injury was
//  sustained by persons or property or otherwise; and whether or not loss was
//  sustained from, or arose out of, the results of, the item, or any services that
//  may be provided by Feabhas.
//
// -------------------------------------------------------------------------------------
#include "fOS_Condition.h"
#include "fOS_Mutex.h"
#include "fOS_Signal.h"
#include "fOS_Time.h"

namespace FeabhOS
{

  void Condition::wait(Mutex& mutex)
  {
    wait_for(mutex, Time::wait_forever);
  }


  bool Condition::wait_for(Mutex& mutex, const Time::Duration& timeout)
  {
    bool timed_out { };
    mutex.unlock();
    timed_out = signal.wait_for(timeout);
    mutex.lock();
    return timed_out;
  }


  void Condition::notify_one()
  {
    signal.notify_one();
  }


  void Condition::notify_all()
  {
    signal.notify_all();
  }

} // namespace FeabhOS
